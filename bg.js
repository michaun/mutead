function createKey(tabid) {
    return "tabMutedState" + tabid;
}

function createAdKey(tabid) {
    return "adsUnmuted" + tabid;
}

function resetIconStorage(tabId) {
    let key = createKey(tabId);
    let adkey = createAdKey(tabId);
    browser.browserAction.setIcon({tabId: tabId, path: "icons/volna.png" });
    browser.storage.local.remove(key);
    browser.storage.local.remove(adkey);
}

function setIconStorage(tabId, setMuted) {
    let key = createKey(tabId);
    let volicon = (setMuted ? "icons/voloff.png" : "icons/volon.png");
    browser.storage.local.set( { [key]: setMuted } );
    browser.browserAction.setIcon({tabId: tabId, path: volicon });
}

function setMute(savedState, adFinished, tab) {
    let key = createKey(tab.id);
    let adkey = createAdKey(tab.id);
    let setState = savedState[key];
    let adsUnmuted = savedState[adkey];
    if (adFinished) {
        if (setState === undefined) { setState = tab.mutedInfo.muted; }
        resetIconStorage(tab.id);
        browser.tabs.update( tab.id, { muted: setState });
    } else {
        if(setState === undefined) { setIconStorage(tab.id, tab.mutedInfo.muted); }
        setState = (adsUnmuted != undefined ? adsUnmuted : true);
        browser.tabs.update( tab.id, { muted: setState });
    }
}

function adjustMute(e) {
    browser.tabs.get(e.tabId).then((tab) => {
        let el = (new URL(e.url)).searchParams.get('el');
        if (el != "adunit") return;
        let isFinal = (new URL(e.url)).searchParams.get('final');
        browser.storage.local.get().then(result =>
            setMute(result, (isFinal === "1"), tab));
    });
}

function storeMuteState(tab) {
    let key = createKey(tab.id);
    browser.storage.local.get().then(result => {
        if(result[key] != undefined)
            setIconStorage(tab.id, !result[key]);
    });
}

function allowAds(result, isMuted, tabid) {
    let key = createKey(tabid);
    if(result[key] != undefined) {
        let adkey = createAdKey(tabid);
        browser.storage.local.set( { [adkey]: isMuted } );
    }
}

function removeStorage(tabId, changeInfo, tabInfo) {
    if(changeInfo.url) {
        browser.storage.local.get().then(result => {
            let key = createKey(tabId);
            if(result[key] != undefined) { browser.tabs.update( tabId, { muted: result[key] }); }
            resetIconStorage(tabId);
        });
    } else if (changeInfo.mutedInfo != undefined && changeInfo.mutedInfo.reason === "user") {
        browser.storage.local.get().then(result =>
            allowAds(result, changeInfo.mutedInfo.muted, tabId));
    }
}

browser.webRequest.onHeadersReceived.addListener(
    adjustMute,
    {urls: ["*://*.youtube.com/api/stats/playback*",
            "*://*.youtube.com/api/stats/watchtime*",
            "*://*.youtube.com/api/stats/qoe*",
            "*://*.youtube.com/get_video_info*"]},
    []
);
browser.browserAction.onClicked.addListener(storeMuteState);
browser.tabs.onUpdated.addListener(removeStorage);

