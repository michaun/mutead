# muteAd

Firefox extension to mute ads on YouTube. Currently it works only with YouTube.\
When ad appears, muteAd mutes the tab containing this ad.\
At the same time it stores tab's "muted" state in browser's local storage (to reinstate it after the ads finish).\
During ad's runtime (and only then), extension's icon becomes active (representing the stored state).\
This icon may be pressed to flip (to-be-set-after-the-ads) "muted" state.

Use at your own risk.